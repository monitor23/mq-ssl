# MQ SSL

This project contains the documentation to enable SSL/TLS encryption on IBM MQ.

The sample document shows how to create a test channel, alter a few queue manager settings, create a keystore and certificate then connect with MQ explorer

Further steps that may be required in the future:

- Enable mutual authentication
- Enable ssl peer checking  of client certificates
- Create multiple channels  each with a unique mca_user
- Create profiles for each mca_user to only allow access sot specific MQ objects
- MQAdvanced - enable encryption of data at rest