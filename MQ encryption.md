# MQ encryption



| Version | Date       | Decscription                                                |
| ------- | ---------- | ----------------------------------------------------------- |
| v0.1    | 22/03/2021 | Initial version                                             |
| v0.2    | 23/03/2021 | Added section to check the settings on channels are correct |
|         |            |                                                             |




## Pre requisite RPMs

The assumption is that the required RPMs are installed for MQ to run and queue managers have been created.  To succesfully enable encryption you need to ensure the GSKIT rpm is installed.

The JRE rpm isn't essential but it does enable the runmqakm command for simpler certificate management

```yml
cd <MQ_download_path>/MQServer
yum install MQSeriesGSKit-<Version>.<Arch>.rpm
yum install MQSeriesJRE-<Version>.<Arch>.rpm
e.g


yum install MQSeriesGSKit-9.1.0-5.x86_64.rpm
yum install MQSeriesJRE-9.1.0-5.x86_64.rpm
```



## Queue Manager settings

First lets create a queue manager and start it

```yml
crtmqm TESTQM
strmqm TESTQM
```

## 

The nest step is to get into the command line interface and set a few parameters

```yml
runmqsc TESTQM

##  Disable some additional checking - we are going to be using certs so don't want user/passwords
alter qmgr connauth('') chlauth(DISABLED)

##  Check the SSL settings
dis qmgr sslkeyr certlabl
#    if certlabl isn't set to "ibmwebspheremq<qmgrname>" then set it
alter qmgr certlabl ('ibmwebspheremqtestqm')
#    if sslkeyr isn't set to /var/mqm/qmgrs/<qmgr_name>/ssl/key then set it
alter qmgr sslkeyr(' /var/mqm/qmgrs/TESTQM/ssl/key')

# Npw lets exit
end
```

## 

## Create listener 

We have a queue manager but at the moment no way for any application to connect to it so we need 2 things - a listener and a channel.

```yml
runmqsc TESTQM

#  Lets define the listener - default ports for MQ are usually 1414 but if you have multiple queue managers on the same box you will need to pick a unique port for each

# In this example we use port 1421 and setting control(qmgr) means if we ever retsart the queue manager  then the listener will be auto started
define listener(TESTQM.LISTENER) TRPTYPE(TCP) PORT(1421) control(qmgr)
# Lets start the listener for the first time
start listener(TESTQM.LISTENER)

# Now lets check it running

dis lsstatus(*) 

#  you should see something like this shopwing status of running which also gives you the Process id (pid)
dis lsstatus(*)
     8 : dis lsstatus(*)
AMQ8631I: Display listener status details.
   LISTENER(TESTQM.LISTENER)                STATUS(RUNNING)
   PID(15986)   

end
```

Just to double check we can always check the process itself outside of mq

```
ps -ef | grep <PID>

e.g.

ps -ef | grep 15986

mqm      15986 15962  0 Mar10 ?        00:00:09 /opt/mqm/bin/runmqlsr -r -m TESTQM -t TCP -p 1421
```





## Create channel

Now we have the listener we want a channel for our application to connect to.  We define these in runmqsc again

```yml
runmqsc TESTQM

# Lets just create the channel basics first then add the extra parameters we want
DEFINE CHANNEL(TESTQM.SVRCONN) CHLTYPE(SVRCONN) TRPTYPE(TCP) DESCR('Just some freeform text to describe this channel') 

dis channel(TESTQM.SVRCONN)
  CHANNEL(TEST.SVRCONN)                    CHLTYPE(SVRCONN)
   ALTDATE(2021-03-10)                     ALTTIME(12.05.24)
   CERTLABL( )                             COMPHDR(NONE)
   COMPMSG(NONE)                           DESCR(Just some freeform text to describe this channel)
   DISCINT(0)                              HBINT(300)
   KAINT(AUTO)                             MAXINST(999999999)
   MAXINSTC(999999999)                     MAXMSGL(10000000)
   MCAUSER()                               MONCHL(QMGR)
   RCVDATA( )                              RCVEXIT( )
   SCYDATA( )                              SCYEXIT( )
   SENDDATA( )                             SENDEXIT( )
   SHARECNV(50)                            SSLCAUTH(REQUIRED)
   SSLCIPH()
   SSLPEER( )                              TRPTYPE(TCP)

# The keys settings we now need to update are mcauser, sslcauth and sslciph

#  MCAUSER tells us what userid we are going to connect as.  It is simplest to set this to the mqm user so we can access all queues, an additional piece of work would be to create channels per application and each would connect as a different user and only have access to their own queues

#  SSLCIPH sets the cipher spec - MQ ojnly likes single cipher specs unliek java applications that usually allow a alist and for both ends to negotiate.  In this test we will use TLS_RSA_WITH_AES_256_CBC_SHA256 whci is a TLS1.2 spec - a full list can be found here  
https://www.ibm.com/support/knowledgecenter/SSFKSJ_9.0.0/com.ibm.mq.sec.doc/q014260_.html

#  SSLCAUTH - will set whether the connecting systems need to send a cert (mutual authentication) so for a first step we will set this to be optional rather than required

ALTER CHANNEL(TESTQM.SVRCONN) CHLTYPE(SVRCONN) MCAUSER('mqm') SSLCAUTH(OPTIONAL)        SSLCIPH(TLS_RSA_WITH_AES_256_CBC_SHA256)


# Lets refresh the security cache - always worth doing this once we change any settings around security or user access

refresh security
end
```



Once the above has been set it is worth displaying the channel to ensure all settings are now as expected

```go
runmqsc TESTQM

dis channel(TESTQM.SVRCONN)  CHLTYPE(SVRCONN)

  CHANNEL(TEST.SVRCONN)                    CHLTYPE(SVRCONN)
   ALTDATE(2021-03-10)                     ALTTIME(12.10.10)
   CERTLABL( )                             COMPHDR(NONE)
   COMPMSG(NONE)                           DESCR(Just some freeform text to describe this channel)
   DISCINT(0)                              HBINT(300)
   KAINT(AUTO)                             MAXINST(999999999)
   MAXINSTC(999999999)                     MAXMSGL(10000000)
   MCAUSER(mqm)   /*MCA user setting*/     MONCHL(QMGR)
   RCVDATA( )                              RCVEXIT( )
   SCYDATA( )                              SCYEXIT( )
   SENDDATA( )                             SENDEXIT( )
   SHARECNV(50)                            SSLCAUTH(OPTIONAL)   /* Chlauth set to optional */
   SSLCIPH(TLS_RSA_WITH_AES_256_CBC_SHA256)   /* Cipher set to the correct value */
   SSLPEER( )                              TRPTYPE(TCP)

end
```



#### Optional steps

It is worthwhile now also creating a CLNTCONN channel in case we need it in future.  We don't need it for MQexplorer or for other queue managers but depending on the appliations, if they are using client jar files they will need to connect using a CCDT (client channel definition table) so ensure they can support TLS

```yml
runmqsc TESTQM

# Define a channel name wth the connname set to the host of hosts where the queue manager is running with the same SSLCIPH as we set for the SVRCONN channel

DEFINE CHANNEL(TESTQM.SVRCONN) CHLTYPE(CLNTCONN) QMNAME(TESTQM) CONNAME('mq.thisservername.co.uk(1421)') +
        SSLCIPH(TLS_RSA_WITH_AES_256_CBC_SHA256) +
        REPLACE

# Don't worry too much about this, using a CCDT is something to discuss when you write your application
end
```





## Keystore set up

Ok, so now we have configured the MQ settings as needed, we now need to set up the keystore and certificate.

As we saw on the qmgr, the sslkeyr path is set to `/var/mqm/qmgrs/TESTQM/ssl/key`

Which means MQ is looking for a file called key.kdb in the /var/mqm/qmgrs/TESTQM/ssl directory.   To simplify things change to that directory which should be empty initially.   

The commands below use the runmqakm command.  If the  MQSeriesJRE rpm is not installed this can be replaced with`/opt/mqm/java/jre64/jre/bin/ikeycmd`

```yml
#  Move to the SSL dir

cd  /var/mqm/qmgrs/TESTQM/ssl

# Create a keystore
runmqakm -keydb -create -db key.kdb -stash -pw password123

# You should now have 3 files, a kdb, a sth and an rdb file
# The kdb should be empty
runmqakm -cert -list -db key.kdb -stashed
`No certificates were found.`

# So lets create a self signed cert
#  the label should be the one set on the qmgr which by default is ibmwebspheremq<qmgrname> so lets stick with that
runmqakm -cert -create -db key.kdb -stashed -label "ibmwebspheremqtestqm" -dn "OU=MQserver,O=TestCo,ST=London,C=GB"

# Then we can list it again
runmqakm -cert -list -db key.kdb -stashed
Certificates found
* default, - personal, ! trusted, # secret key
-	ibmwebspheremqtestqm

#  We may now want to check the details such as the expiry date of the certificate
runmqakm -cert -details -label ibmwebspheremqtestqm -db key.kdb -stashed

#  This produces a bunch of output, the important ones that catch people out are the valid from and valid to dates, outisde if these and things stop working

Not Before : 21 March 2021 13:14:46 GMT+00:00
Not After : 22 March 2022 13:14:46 GMT+00:00


#  As we are using self signed we need to extratc the public cert to share with connecting applications
runmqakm -cert -extract -label ibmwebspheremqtestqm -db key.kdb -stashed -file /tmp/testqm.cert

```



## MQ explorer set up

The first step to set up in MQ explorer is out of the scope of this document.  You need to download the extracted certificate to the machine you are running MQ explorer.  From there you need to create a keystore (preferably jks) and add the certificate into it.

Once done we can now add the queue manager in MQ explorer.

Right click on "Queue Managers" and click "Add Remote Queue Manager"

Enter the queue manager name and ensure the "Connect Directly" radio button is selected

![image-20210322135026994](C:\Work\DMS\mqssl\image-20210322135026994.png)



On the next screen enter your host or IP address as well as the port (1421 in this case) and the SVRCONN name set up earlier

![image-20210322135246744](C:\Work\DMS\mqssl\image-20210322135246744.png)

Click next.  On the security exit and the user identity screen screen, leave all settings as they are and click next till you get to the screen titled "Specify SSL certificate and key repository details"

On this screen check the box "Enable SSL key repositories".  Use the browse button to locate the local keystore where we stored the extracted MQ certificate.  You can add the keystore password in as well or wait for the prompt later



![image-20210322142135111](C:\Work\DMS\mqssl\image-20210322142135111.png)



On the next screen check the box that is labelled "Enable SSL Options" and from the SSL CipherSpec dropdown, set this to the same cipher spec as set up in MQ on the channel earlier -  TLS_RSA_WITH_AES_256_CBC_SHA256 

![image-20210322142403257](C:\Work\DMS\mqssl\image-20210322142403257.png)



Then click finish.  All being well you should connect successfully.  If not the best place to look for errors is on the MQ server under `/var/mqm/qmgrs/TESTQM/errors` in file `AMQERR01.LOG`